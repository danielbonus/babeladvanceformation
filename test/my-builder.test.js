import { fixture, html, expect } from '@open-wc/testing';
import '../src/index.js';

describe('my first test', () => {
  let el;
  beforeEach(() => {
    el = fixture(html` <my-app-builder></my-app-builder> `);
  });

  it('my first case', () => {
    expect(el).to.exist;
  });
});
