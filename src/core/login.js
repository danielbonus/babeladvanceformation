import firebase from 'firebase';

const INITIAL_CONFIG_FIREBASE = {
  apiKey: "AIzaSyB7TtFpOaoSosC3S3eDXTjBf6cV6lkNVDk",
  authDomain: "babel-formation.firebaseapp.com",
  projectId: "babel-formation",
  storageBucket: "babel-formation.appspot.com",
  messagingSenderId: "628321860251",
  appId: "1:628321860251:web:cd8e2ec6adf61bfae8243e",
  measurementId: "G-XMYVYYLGBV"
}

class Login {
  constructor() {
    this.isLogin = false;
    this.firebaseInit = firebase.initializeApp(INITIAL_CONFIG_FIREBASE);
    this.auth = this.firebaseInit.auth();
    this.auth.onAuthStateChanged( user => {
      this.isLogin = user !== null
    });
  }

  isLoggedInPromise() {
    return new Promise((resolve, reject) => {
      this.auth.onAuthStateChanged( user => {
        return resolve(user);
      });
    });
  }

  _tokenSessionStorage(data) {
    if(this.isLogin && data) {
      sessionStorage.setItem('tokenLogin', JSON.stringify(data));
    } else {
      sessionStorage.removeItem('tokenLogin');
    }
  }

  async registerUser(data) {
    let _isRegister;
    try {
      _isRegister = !!await this.auth.createUserWithEmailAndPassword(data.email, data.password);
    }catch (err) {
      _isRegister = false;
    }
    return _isRegister
  }


  async singOut() {
    return !await this.auth.signOut();
  }


}
export default new Login();
