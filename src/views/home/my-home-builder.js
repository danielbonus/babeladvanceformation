import { LitElement, html, css } from 'lit-element';

export class MyHomeBuilder extends LitElement {
  static get styles() {
    return css `
    `
  }

  static get properties() {
    return {
    }
  }

  constructor() {
    super();
  }

  _handleLogout(event) {
    this.dispatchEvent( new CustomEvent('on-logout', { bubbles: true, composed: true}))
  }

  render() {
    return html `
        <h2>soy home</h2>
        <button @click="${this._handleLogout}">Logout</button>
    `
  }

}
