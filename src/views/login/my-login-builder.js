import { LitElement ,html, css } from 'lit-element';

export class MyLoginBuilder extends LitElement {
  static get styles() {
    return css `
    `
  }

  constructor() {
    super();
  }

  static get properties() {
    return {

    }
  }

  _handleAuthentication(event) {
    event.preventDefault();
    this.dispatchEvent( new CustomEvent('on-login', {
      bubbles: true,
      composed: true,
      detail: {
        email: event.currentTarget.querySelectorAll('input[name="email"]')[0].value,
        password: event.currentTarget.querySelectorAll('input[name="password"]')[0].value,
      }
    }));
  }

  _handleLogout() {
    this.dispatchEvent( new CustomEvent('on-logout', { bubbles: true, composed: true}))
  }

  render() {
    return html `
        <form @submit=${this._handleAuthentication}>
          <input name="email" type="text">
          <input name="password" type="password">
          <input type="submit">
        </form>
    `
  }

}
