import { LitElement, html, css } from 'lit-element';
import { RouterProvider} from "../../router/routerMixin";

export class MyProfileBuilder extends RouterProvider(LitElement) {
  constructor() {
    super();
  }

  _handleClick(event) {
    this.navigator('/home');
  }

  render() {
    return html `
        <h2>Profile</h2>
        <button @click="${this._handleClick}">home</button>
    `
  }
}
