export const pathRoutes = [
  {
    path: '/',
    component: 'my-login-builder',
    protected: false,
    componentPath: '../views/login/myLoginBuilder.js'
  },
  {
    path: '/home',
    component: 'my-home-builder',
    protected: true,
    componentPath: '../views/home/myHomeBuilder.js'
  },
  {
    path: '/profile',
    component: 'my-profile-builder',
    protected: true,
    componentPath: '../views/profile/myProfileBuilder.js'
  },
];
