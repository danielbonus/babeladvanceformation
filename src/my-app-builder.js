import { LitElement, css, html } from 'lit-element';
import { RouterMixin, RouterProvider } from "./router/routerMixin";
import { pathRoutes } from "./router/routes";
import Login from './core/login';
import './views/login/myLoginBuilder.js';

export class myAppBuilder extends RouterMixin(RouterProvider(LitElement)) {
  static get properties() {
    return {
      myTitle: { type: String},
      isLogin: { type: Boolean },
    };
  }


  static get styles() {
    return css`

    `;
  }

  get routes() {
    return pathRoutes
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('on-login', this._handleAuthentication);
    this.addEventListener('on-logout', this._handleSingOutAuthentication);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('on-login', this._handleAuthentication);
    this.removeEventListener('on-logout', this._handleSingOutAuthentication)
  }

  async _handleAuthentication({ detail }) {
    this.isLogin = await Login.registerUser(detail)
    if(this.isLogin) {
      this.navigator('/home');
    }
  }

  async _handleSingOutAuthentication() {
    if(await Login.singOut()) {
      this.navigator('/');
    }
  }

  constructor() {
    super();
    this.myTitle = 'My App';
    this.isLogin = false;
  }

  render() {
    return html`
     <section>
       <nav></nav>
       <main id="content-router">
       </main>
     </section>
    `;
  }
}
