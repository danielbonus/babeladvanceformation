import merge from 'deepmerge';
import copy from 'rollup-plugin-copy';
import { createSpaConfig } from '@open-wc/building-rollup';

const baseConfig = createSpaConfig({
  legacyBuild: true,
  developmentMode: false,
  injectServiceWorker: true
});

export default merge(baseConfig, {
  input: './index.html',
  plugins: [
    copy({
      targets: [
        {
          src: './assets', dest: 'dist'
        }
      ]
    })
  ]
});
