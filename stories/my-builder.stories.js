import { html } from 'lit-element';
import '../src/index.js';

export default {
  title: 'my-app-builder',
  component: 'my-app-builder',
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

function Template({ title, backgroundColor }) {
  return html`
    <my-app-builder
    >
    </my-app-builder>
  `;
}

export const App = Template.bind({});
App.args = {
  title: 'My app',
};
